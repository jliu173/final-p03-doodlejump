//
//  Doodle.m
//  P03-Doodlejump
//
//  Created by 刘江韵 on 2017/2/18.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#import "Doodle.h"

@interface Doodle()
@property (nonatomic, strong) CADisplayLink *displayLink;

@end

@implementation Doodle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"doodle.jpg"] ];
    _displayLink = [CADisplayLink displayLinkWithTarget:_gameView selector:@selector(arrange:)];
    [_displayLink setPreferredFramesPerSecond:30];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    NSLog(@"SB");
    // Do any additional setup after loading the view, typically from a nib.

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(IBAction)speedChange:(id)sender
//{
//    UISlider *s = (UISlider *)sender;
//    // NSLog(@"tilt %f", (float)[s value]);
//    [self.gameView setTilt:(float)[s value]];
//}

-(IBAction)Sbleft:(id)sender
{
    //UISlider *s = (UISlider *)sender;
    // NSLog(@"tilt %f", (float)[s value]);
    [self.gameView setTilt:-4];
}
-(IBAction)Sbright:(id)sender
{
    //UISlider *s = (UISlider *)sender;
    // NSLog(@"tilt %f", (float)[s value]);
    [self.gameView setTilt:4];
}
@end

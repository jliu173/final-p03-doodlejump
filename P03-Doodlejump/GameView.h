//
//  GameView.h
//  P03-Doodlejump
//
//  Created by 刘江韵 on 2017/2/25.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "Brick.h"

@interface GameView : UIView {
    
}
@property (nonatomic, strong) Jumper *jumper;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic) float tilt;
@property(nonatomic,strong) IBOutlet UILabel *message;
-(void)arrange:(CADisplayLink *)sender;

@end

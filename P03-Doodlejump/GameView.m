//
//  GameView.m
//  P03-Doodlejump
//
//  Created by 刘江韵 on 2017/2/25.
//  Copyright © 2017年 刘江韵. All rights reserved.
//
#import "GameView.h"

@implementation GameView
@synthesize jumper, bricks;
@synthesize tilt;

int score = 0;


-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        CGRect bounds = [self bounds];
        [_message setText:[NSString stringWithFormat:@"%d",0]];
        jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 60, 60, 60)];
        //[jumper setBackgroundColor:[UIColor redColor]];
        
        UIGraphicsBeginImageContext(jumper.frame.size);
        [[UIImage imageNamed:@"panda.png"] drawInRect:jumper.bounds];
        UIImage *panda = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        jumper.backgroundColor = [UIColor colorWithPatternImage:panda];
        
        [jumper setDx:0];
        [jumper setDy:10];
        [self addSubview:jumper];
        [self makeBricks:nil];
    }
    return self;
}

-(IBAction)makeBricks:(id)sender
{
    CGRect bounds = [self bounds];
    float width = bounds.size.width * .5;
    float height = 25;
    
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    
    bricks = [[NSMutableArray alloc] init];
    for (int i = 0; i < 10; ++i)
    {
        Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        //[b setBackgroundColor:[UIColor blueColor]];
        UIGraphicsBeginImageContext(b.frame.size);
        [[UIImage imageNamed:@"Brick.png"] drawInRect:jumper.bounds];
        UIImage *brick = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        b.backgroundColor = [UIColor colorWithPatternImage:brick];

        [self addSubview:b];
        [b setCenter:CGPointMake(rand() % (int)(bounds.size.width * .8), rand() % (int)(bounds.size.height * .8)+(int)(bounds.size.height * .2))];
        [bricks addObject:b];
    }
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(void)arrange:(CADisplayLink *)sender
{
    CFTimeInterval ts = [sender timestamp];
    
    CGRect bounds = [self bounds];
    
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    // Apply the tilt.  Limit maximum tilt to + or - 5
    [jumper setDx:tilt];
    if ([jumper dx] > 5)
        [jumper setDx:5];
    if ([jumper dx] < -5)
        [jumper setDx:-5];
    
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    p.x += [jumper dx];
    p.y -= [jumper dy];
    
    // If the jumper has fallen below the bottom of the screen,
    // add a positive velocity to move him
    if (p.y > bounds.size.height)
    {
        [jumper setDy:10];
        p.y = bounds.size.height;
    }
    
    // If we've gone past the top of the screen, wrap around
    if (p.y < 0)
        p.y += bounds.size.height;
    
    // If we have gone too far left, or too far right, wrap around
    if (p.x < 0)
        p.x += bounds.size.width;
    if (p.x > bounds.size.width)
        p.x -= bounds.size.width;
    
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    if ([jumper dy] < 0)
    {
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            if (CGRectContainsPoint(b, p))
            {
                score+=10;
                [_message setText:[NSString stringWithFormat:@"%d",score]];
                // Yay!  Bounce!
                NSLog(@"Bounce!");
                [jumper setDy:10];
                [jumper setDx:0];
            }
        }
        
    }
    
    [jumper setCenter:p];
    NSLog(@"Timestamp %f", ts);
}

@end










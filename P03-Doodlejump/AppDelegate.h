//
//  AppDelegate.h
//  P03-Doodlejump
//
//  Created by 刘江韵 on 2017/2/18.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


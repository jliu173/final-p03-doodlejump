//
//  Jumper.h
//  P03-Doodlejump
//
//  Created by 刘江韵 on 2017/2/25.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Jumper : UIView
@property (nonatomic) float dx, dy;  // Velocity
@end
